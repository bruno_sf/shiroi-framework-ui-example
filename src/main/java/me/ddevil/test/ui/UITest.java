package me.ddevil.test.ui;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class UITest extends JavaPlugin {
    private UIMenu menu;

    @Override
    public void onEnable() {
        this.menu = new UIMenu(this);
        menu.setup();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase("uitest")) {
            menu.open((Player) sender);
        }
        return true;
    }
}
