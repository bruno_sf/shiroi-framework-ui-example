package me.ddevil.test.ui;

import me.ddevil.shiroi.ui.api.Item;
import me.ddevil.shiroi.ui.api.Menu;
import me.ddevil.shiroi.ui.internal.container.ArrayContainer;
import me.ddevil.shiroi.ui.internal.menu.AutonomousMenu;
import me.ddevil.shiroi.ui.internal.misc.BasicItem;
import me.ddevil.shiroi.ui.internal.misc.CloseButton;
import me.ddevil.shiroi.ui.internal.misc.ItemClickable;
import me.ddevil.shiroi.ui.internal.scrollable.LowPanedScrollable;
import me.ddevil.shiroi.ui.internal.scrollable.updater.BasicScrollableUpdater;
import me.ddevil.shiroi.ui.misc.UIPosition;
import me.ddevil.shiroi.util.item.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;

public class UIMenu extends AutonomousMenu<JavaPlugin> {

    private static final int MAX_BLOCKS = 448;

    public UIMenu(JavaPlugin plugin) {
        super(plugin, "Oin", 6);
    }

    /**
     * Called only on {@link Menu#setup()}
     */
    @Override
    protected void setup1() {

        LowPanedScrollable<Item<?>> panel = new LowPanedScrollable<>(5, 4, new BasicScrollableUpdater<>(new ItemStack(Material.EMERALD), "Next", "Previous", ChatColor.GREEN, ChatColor.DARK_GREEN, ChatColor.DARK_GRAY), this);
        panel.goToPage(4);
        ArrayContainer<Item<?>> container = new ArrayContainer<>(2, 4, this);
        this.place(container, new UIPosition(6, 0));
        container.setBackground(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15));
        for (int i = 0; i < 5; i++) {
            container.add(new BasicItem<>(container, new ItemStack(Material.LAPIS_ORE)));
        }
        panel.setBackground(new ItemStack(Material.GOLD_BLOCK));
        this.place(panel, new UIPosition(0, 0));
        int currentId = 0;
        int totalPlacedBlocks = 0;
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            if (currentId > MAX_BLOCKS) {
                break;
            }
            Material m = Material.getMaterial(currentId);
            if (m != null) {
                if (m.isSolid()) {
                    int finalTotalPlacedBlocks = totalPlacedBlocks;
                    panel.place(new ItemClickable<>(
                                    new ItemBuilder(m, null).setName("Block n°" + totalPlacedBlocks + " (id " + currentId + " | " + m.name() + ")").toItemStack(),
                                    uiClickEvent -> uiClickEvent.getPlayer().sendMessage(ChatColor.DARK_GRAY + "You clicked block n°" + ChatColor.GREEN + finalTotalPlacedBlocks),
                                    this),
                            totalPlacedBlocks);
                    totalPlacedBlocks++;
                }
            }
            currentId++;
        }
        panel.setLowPanelBackground(new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 13));
        panel.placeInLowPanel(new BasicItem<>(panel, new ItemBuilder(Material.BOOK, null)
                .setName(ChatColor.GRAY + "There is a total of " + ChatColor.GREEN + totalPlacedBlocks + ChatColor.GRAY + " blocks displayed in here!")
                .setLore(Arrays.asList(
                        ChatColor.GRAY + "There are " + ChatColor.GREEN + panel.getTotalPages() + ChatColor.GRAY + " pages!",
                        ChatColor.GRAY + "This panel occupies " + ChatColor.GREEN + panel.getSize() + ChatColor.GRAY + " slots",
                        ChatColor.GRAY + "and can display " + ChatColor.GREEN + panel.getPageSize() + ChatColor.GRAY + " objects per page"
                        )
                )
                .toItemStack()), 2);
        this.place(new CloseButton<>(new ItemStack(Material.DIAMOND), this), new UIPosition(0, 5));
        setBackground(new ItemStack(Material.IRON_FENCE));
    }


    @Override
    protected void update0() {

    }
}
